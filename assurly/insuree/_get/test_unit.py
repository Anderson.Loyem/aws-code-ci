from lambda_function import service

def test_service():
    print("Test lambda insuree post")
    data = {
        "insuree_id": "001"
    }
    response = {'statusCode': 200, 'body': {'version': 'v2', 'success': True, 'error_code': 200, 'message': None, 'data': {'email': 'andylez@kmer.cm', 'insuree_id': '001'}}}
    assert service(data) == response

### main function main
if __name__ == '__main__':
    test_service()
