# description : get-insuree
inserer un device (terminal) dans dyanamoBD

## requirement
NA

## API Gateway : assurly_app
<url_base>/insuree {POST}

## data input format
{
  "product": "product",
 }

## data output format
{
  "statusCode": 500,
  "body": {
    "version": "v2",
    "success": false,
    "error_code": "An error occurred (ValidationException) when calling the PutItem operation: One or more parameter values were invalid: Missing the key email in the item",
    "message": null,
    "data": null
  }
}
