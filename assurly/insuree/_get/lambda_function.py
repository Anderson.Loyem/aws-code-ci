import boto3
from boto3.dynamodb.conditions import Key
# init ressources
dynamodb = boto3.client('dynamodb')
dynamodb2 = boto3.resource('dynamodb')
TABLE_NAME = "insuree"
table = dynamodb2.Table(TABLE_NAME)
# test 2 yesssss
def service(insuree):
    try:
        print(insuree)
        for field in ["insuree_id"]:
            if not insuree.get(field):
                message = f"{field} is not present"
                print(message)
                return {
                    "statusCode": 500,
                    "body": dict(version="v2", success=True, error_code=500, message=message, data=None)
                }
        item = table.query(KeyConditionExpression=Key('insuree_id').eq(insuree['insuree_id'])).get('Items')
        if (len(item) != 0) :
            response = {
                "statusCode": 200,
                "body": dict(version="v2", success=True, error_code=200, message=None, data=item[0])
            }
            print(response)
            return response
        else :
            response = {
                "statusCode": 400,
                "body": dict(version="v1", success=False, error_code=400, message="the insuree's id was not found.", data=None)
            }
            print(response)
            return response

    except Exception as err:
        response = {
            "statusCode": 500,
            "body": dict(version="v2", success=False, error_code=500, message=str(err), data=None)
        }
        print(response)
        return response


def lambda_handler(event, context):
    print(context)
    insuree = event
    return service(insuree)

if __name__ == '__main__':
    insuree = {
        "insuree_id": "001"
    }
    print(service(insuree))
