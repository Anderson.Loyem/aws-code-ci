import boto3
# init ressources
dynamodb = boto3.client('dynamodb')
dynamodb2 = boto3.resource('dynamodb')
TABLE_NAME = "insuree"
table = dynamodb2.Table(TABLE_NAME)

def service(data):
    try:
        print(data)
        for field in ["insuree_id", "email"]:
            if not data.get(field):
                message = f"{field} is not present"
                print(message)
                return {
                    "statusCode": 500,
                    "body": dict(version="v2", success=False, error_code=message, message=message, output=None)
                }
        table.put_item(Item=data)
        response = {
            "statusCode": 200,
            "body": dict(version="v2", success=True, error_code=200, message=None, output=None)
        }
        print(response)
        return response

    except Exception as err:
        response = {
            "statusCode": 500,
            "body": dict(version="v2", success=False, error_code=500, message=str(err), output=None)
        }
        print(response)
        return response


def lambda_handler(event, context):
    print(context)
    data = event
    return service(data)

if __name__ == '__main__':
    insuree = {
        "serverId": "201"
    }
    print(service(insuree))
