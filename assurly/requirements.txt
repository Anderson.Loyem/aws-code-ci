boto3==1.14.40
requests==2.24.0
botocore==1.17.40
httpx==0.14.3
jsonschema==3.2.0
awscli==1.18.117
py==1.8.2
pylint==2.9.3
pytest==6.1.2
pytz==2020.1
urllib3==1.25.10
autopep8==1.5.7
bandit==1.7.0
coverage==5.5

